#ifndef __DBBASECLASS_SQLAPI_H_
#define __DBBASECLASS_SQLAPI_H_

#include "StdAfx.h"

//////////////////////////////////////////////////////////////////////////////////
// Base class for handling database transactions using SQLApi; 061031 p�d

// Structrue for Sending/Reciving SQLApi++ object connmection data
// from HMSShell to Suit/User module, using WM_COPYDATA; 010719 p�d
typedef struct _db_connection_data
{
	SAConnection* conn;
	SAConnection* admin_conn;
} DB_CONNECTION_DATA;

class CDBBaseClass_SQLApi
{
//private:

		//	Specifies the database to connect to
		//	SA_Client_NotSpecified,
		//	SA_ODBC_Client,
		//	SA_Oracle_Client,
		//	SA_SQLServer_Client,
		//	SA_InterBase_Client,
		//	SA_SQLBase_Client,
		//	SA_DB2_Client,
		//	SA_Informix_Client,
		//	SA_Sybase_Client,
		//	SA_MySQL_Client,
		//	SA_PostgreSQL_Client,
		SAClient_t m_saClient;	

		SAString m_sDataBase;
		SAString m_sUserName;
		SAString m_sPassword;

		enum CONNECTION_TYPES
					{ CONNECTED_TYPE_NOT_CONNECTED,
					  CONNECTED_TYPE_FROM_MAIN_PROGRAM,	// Use connection in Main program
			      CONNECTED_TYPE_BY_LOGIN						// Connected using Login info
					};

	protected:
		SAConnection m_saConnection;	// SqlAPI++ method
		SACommand m_saCommand;				// SqlAPI++ method

		DB_CONNECTION_DATA m_dbConnectionData;
		CONNECTION_TYPES m_enumConnType;
		int m_nConnTypeNumber;
	public:
		CDBBaseClass_SQLApi(void);
		virtual ~CDBBaseClass_SQLApi(void);

		virtual BOOL exists(LPCTSTR sql);
		virtual BOOL db_exists(LPCTSTR db_name);
		virtual BOOL table_exists(LPCTSTR db_name,LPCTSTR table_name);

		virtual long num_of_records(LPCTSTR table_name);

		virtual int last_identity(LPCTSTR table_name,LPCTSTR column_name = _T(""));

		CDBBaseClass_SQLApi(SAClient_t client,LPCTSTR db_name = _T(""),LPCTSTR user_name = _T(""),LPCTSTR psw = _T(""));
		CDBBaseClass_SQLApi(DB_CONNECTION_DATA &db_connection,int type_of_conn);


		//methods
		DB_CONNECTION_DATA &getDBConnection(void);

		void doCommit(void);
		void doRollback(void);

		BOOL connectToDataBase(void);
		BOOL connectToDataBase(SAConnection *con);

		SAClient_t getSAClient(void);
		SAString getDataBase(void);
		SAString getUserName(void);
		SAString getPassword(void);

		SAString convertSADateTime(SADateTime &);

		int getConnTypeNumber(void);



};

#endif
