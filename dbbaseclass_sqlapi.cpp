#include "StdAfx.h"
#include "DBBaseClass_SQLApi.h"


/////////////////////////////////////////////////////////////////////////////////////////
// CDBBaseClass_SQLApi

// PUBLIC CONSTRUCTORS
CDBBaseClass_SQLApi::CDBBaseClass_SQLApi(void)
{
	m_saClient	= SA_Client_NotSpecified;
	m_sDataBase	= "";
	m_sUserName	= "";
	m_sPassword	= "";
	m_enumConnType = CONNECTED_TYPE_NOT_CONNECTED;
	m_nConnTypeNumber = -1;
}


CDBBaseClass_SQLApi::CDBBaseClass_SQLApi(SAClient_t client,LPCTSTR db_name,LPCTSTR user_name,LPCTSTR psw)
{
	m_saClient	= client;
	m_sDataBase	= (wchar_t*)db_name;
	m_sUserName	= (wchar_t*)user_name;
	m_sPassword	= (wchar_t*)psw;
	m_enumConnType = CONNECTED_TYPE_BY_LOGIN;
	m_nConnTypeNumber = -1;
}

CDBBaseClass_SQLApi::CDBBaseClass_SQLApi(DB_CONNECTION_DATA &db_connection,int type_of_conn)
{
	m_saClient	= db_connection.conn->Client();
	m_nConnTypeNumber = type_of_conn;
	try
	{
		if (type_of_conn == 1)
		{
			m_dbConnectionData = db_connection;
			if (m_dbConnectionData.conn->isConnected())
			{
				m_saCommand.setConnection(m_dbConnectionData.conn);
				m_enumConnType = CONNECTED_TYPE_FROM_MAIN_PROGRAM;
			}
		}
		else if (type_of_conn == 2)
		{
			m_dbConnectionData = db_connection;
			if (m_dbConnectionData.admin_conn->isConnected())
			{
				m_saCommand.setConnection(m_dbConnectionData.admin_conn);
				m_enumConnType = CONNECTED_TYPE_FROM_MAIN_PROGRAM;
			}
		}
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
	}
}

// PUBLIC DESTRUCTOR
CDBBaseClass_SQLApi::~CDBBaseClass_SQLApi(void)
{

	// Disconnect from database; 060105 p�d
	if (m_saConnection.isConnected())
	{
		m_saConnection.Disconnect();
	}

}

// Public methods
DB_CONNECTION_DATA &CDBBaseClass_SQLApi::getDBConnection(void)
{
	return m_dbConnectionData;
}

void CDBBaseClass_SQLApi::doCommit(void)
{
	if (m_enumConnType == CONNECTED_TYPE_FROM_MAIN_PROGRAM)
	{
		if (m_nConnTypeNumber == 1)
		{
			m_dbConnectionData.conn->Commit();
		}
		else if (m_nConnTypeNumber == 2)
		{
			m_dbConnectionData.admin_conn->Commit();
		}
	}
	else if (m_enumConnType == CONNECTED_TYPE_BY_LOGIN)
		m_saConnection.Commit();
}

void CDBBaseClass_SQLApi::doRollback(void)
{
	if (m_enumConnType == CONNECTED_TYPE_FROM_MAIN_PROGRAM)
	{
		if (m_nConnTypeNumber == 1)
		{
			m_dbConnectionData.conn->Rollback();
		}
		else if (m_nConnTypeNumber == 2)
		{
			m_dbConnectionData.admin_conn->Rollback();
		}
	}
	else if (m_enumConnType == CONNECTED_TYPE_BY_LOGIN)
		m_saConnection.Rollback();
}

BOOL CDBBaseClass_SQLApi::connectToDataBase(void)
{
	try
	{
		m_saConnection.Connect(getDataBase(),getUserName(),getPassword(),getSAClient());
		
		if (m_saConnection.isConnected())
		{
			m_saCommand.setConnection(&m_saConnection);
		}
	}
	catch(SAException &)
	{
		return FALSE;
	}
	return TRUE;
}

BOOL CDBBaseClass_SQLApi::connectToDataBase(SAConnection *con)
{
	try
	{
		if (m_saConnection.isConnected())
		{
			m_saCommand.setConnection(&m_saConnection);
		}
	}
	catch(SAException &)
	{
		return FALSE;
	}
	return TRUE;
}

BOOL CDBBaseClass_SQLApi::exists(LPCTSTR sql)
{
	BOOL bExists = FALSE;
	int nCount = 0;
	try
	{
		if (m_nConnTypeNumber == 1)
			m_saCommand.setConnection(getDBConnection().conn);
		else if (m_nConnTypeNumber == 2)
			m_saCommand.setConnection(getDBConnection().admin_conn);

		m_saCommand.setCommandText((SAString)(sql));
		m_saCommand.Execute();
		
		while (m_saCommand.FetchNext())	nCount++;
		bExists = (nCount > 0);
	}
	catch(SAException &e)
	{
	/*
		CString S;
		//if (e.ErrClass() == SA_DBMS_API_Error)
			S.Format(L"%s\nCode %d\nErrClass  %d",e.ErrText(),e.ErrNativeCode(),e.ErrClass());
			// print error message
			UMMessageBox(S);
		//}
	*/

		if (m_nConnTypeNumber == 1)
			getDBConnection().conn->Rollback();
		else if (m_nConnTypeNumber == 2)
			getDBConnection().admin_conn->Rollback();

		UMMessageBox(e.ErrText());

		return TRUE;	// Return TRUE on error = Empty; 060111 p�d
	}

	return bExists;
}

BOOL CDBBaseClass_SQLApi::db_exists(LPCTSTR db_name)
{
	SAString sSQL;
	BOOL bFound		= FALSE;
	CString sDB(db_name);
	CString sInDB;
	try
	{
		// Check if database exist; 091028 Peter
		sSQL.Format(_T("IF DB_ID('%s') IS NOT NULL SELECT 1"), db_name);
		m_saCommand.setCommandText(sSQL);
		m_saCommand.Execute();
		if(m_saCommand.isResultSet())
		{
			bFound = TRUE;
		}	// if(m_saCommand.FetchNext())

	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;	// Return TRUE on error = Empty; 060111 p�d
	}

	return bFound;
}

// Check if a tables exists in a db; 060313 p�d
BOOL CDBBaseClass_SQLApi::table_exists(LPCTSTR db_name,LPCTSTR table_name)
{
	BOOL bIsOK = FALSE;

	CString sSQL;
	try
	{
		// Check which database server's used; 060313 p�d
		if (m_saClient == SA_MySQL_Client)
		{
			sSQL.Format(_T("select * from `%s`.`%s`;"),db_name,table_name);
		}
		else if (m_saClient == SA_SQLServer_Client)
		{
			sSQL.Format(_T("use %s select name from sysobjects where name='%s';"),db_name,table_name);
		}
		m_saCommand.setCommandText((SAString)(sSQL));
		m_saCommand.Execute();
		bIsOK = TRUE;
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return FALSE;
	}

	return bIsOK;
}

long CDBBaseClass_SQLApi::num_of_records(LPCTSTR table_name)
{
	CString sSQL;

	long nNumOf = -1;

	try
	{
		sSQL.Format(_T("select count(*) from %s"),table_name);
		m_saCommand.setCommandText((SAString)(sSQL));
		m_saCommand.Execute();

		while(m_saCommand.FetchNext())
		{
			nNumOf = m_saCommand.Field(1).asLong();
		}
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return nNumOf;
	}

	return nNumOf;
}

int CDBBaseClass_SQLApi::last_identity(LPCTSTR table_name,LPCTSTR column_name)
{
	CString sSQL,S;
	CString sIdentity;
	int nIdentity = 0;
	try
	{
//		if (getConnTypeNumber() == 1)	// SQL Server
		if (m_saClient == SA_SQLServer_Client)
		{
			sSQL.Format(_T("select ident_current(\'%s\') as Ident"),table_name);
		}
//		else if (getConnTypeNumber() == 2)	// MySQL
		else if (m_saClient == SA_MySQL_Client)
		{
			sSQL.Format(_T("select max(%s) as Ident from %s"),column_name,table_name);
		}
		m_saCommand.setCommandText((SAString)(sSQL));
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			if (m_saClient == SA_SQLServer_Client ||
					m_saClient == SA_MySQL_Client)
			{
				nIdentity =	m_saCommand.Field("Ident").asShort();
			}
		}
	}
	catch(SAException &e)
	{
		UMMessageBox(e.ErrText());
		return nIdentity;
	}
	return nIdentity;
}


SAClient_t CDBBaseClass_SQLApi::getSAClient(void)
{
	return m_saClient;
}

SAString CDBBaseClass_SQLApi::getDataBase(void)
{
	return m_sDataBase;
}

SAString CDBBaseClass_SQLApi::getUserName(void)
{
	return m_sUserName;
}

SAString CDBBaseClass_SQLApi::getPassword(void)
{
	return m_sPassword;
}

SAString CDBBaseClass_SQLApi::convertSADateTime(SADateTime &dt)
{
	TCHAR szTmp[32];

	_stprintf(szTmp,_T("%d-%02d-%02d %02d:%02d:%02d.%d"),
		dt.GetYear(),
		dt.GetMonth(),
		dt.GetDay(),
		dt.GetHour(),
		dt.GetMinute(),
		dt.GetSecond(),
		dt.Fraction()/1000000);	// To get milliseconds; 090224 p�d

	return szTmp;
}


int CDBBaseClass_SQLApi::getConnTypeNumber(void)
{
	if (m_saClient == SA_SQLServer_Client) return 1;
	else if (m_saClient == SA_MySQL_Client) return 2;
	else if (m_saClient == SA_Oracle_Client) return 3;
	else return 0;
}

