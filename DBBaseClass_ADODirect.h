#ifndef __DBBASECLASS_ADODIRECT_H_
#define __DBBASECLASS_ADODIRECT_H_

#include "StdAfx.h"


// This DLL for ADO DBhandling; 061031 p�d
//#import "msado15.dll" no_namespace rename("EOF", "EndOfFile")
#import "msado15.dll" rename("EOF", "EndOfFile") 
using namespace ADODB;

/////////////////////////////////////////////////////////////////////////
// CDBBaseClass_ADODirect
class CDBBaseClass_ADODirect
{
	//private:
	_ConnectionPtr m_pConnection;
	_CommandPtr m_pCommand;
	_RecordsetPtr m_pRecSet;

	CString m_sErrorMessage;
	HRESULT hr;
	void setExceptionMsg(_com_error &e);

	BOOL isEOF(void);
public:
	CDBBaseClass_ADODirect(void);
	~CDBBaseClass_ADODirect(void);

	CString getErrorMessage(void);

	BOOL setupConnectionToDatabase(LPCTSTR);

	void setDefaultDatabase(LPCTSTR);
	_bstr_t getDefaultDatabase(void);

	BOOL connectionExecute(LPCTSTR);

	BOOL commandExecute(LPCTSTR);

	BOOL recordsetOpenTable(LPCTSTR);
	BOOL recordsetQuery(LPCTSTR);
	BOOL recordsetRequery(void);
	void recordsetClose(void);
	void recordsetAddNew(void);
	void recordsetUpdate(void);

	long getNumOf(void);


	virtual CString getStr(LPCTSTR);
	virtual void addStr(LPCTSTR,LPCTSTR);
	virtual int getInt(LPCTSTR);
	virtual void addInt(LPCTSTR,int);

	// Move around transactions
	void firstRec(void);
	BOOL nextRec(void);
};


/////////////////////////////////////////////////////////////////////////
// CSQLServer_ADODirect
class CSQLServer_ADODirect : public CDBBaseClass_ADODirect
{
public:
	CSQLServer_ADODirect(void);

	BOOL connectToDatabase(int authentication,
												 LPCTSTR user_id,
												 LPCTSTR psw,
												 LPCTSTR data_source,	// e.g. PADDATOR\\SQLEXPRESS
												 LPCTSTR init_catalog = _T(""));	// e.g. gallbas
	BOOL existsDatabase(LPCTSTR db_name);
	BOOL existsTableInDB(LPCTSTR db_name,LPCTSTR table_name);
	BOOL existsColumnInTableInDB(LPCTSTR db_name,LPCTSTR table_name,LPCTSTR column_name);
	BOOL dropTableInDB(LPCTSTR db_name,LPCTSTR table_name);

};


/////////////////////////////////////////////////////////////////////////
// CAccess_ADODirect
class CAccess_ADODirect : public CDBBaseClass_ADODirect
{
public:
	CAccess_ADODirect(void);

	BOOL connectToDatabase(LPCTSTR db_name);
};


#endif
